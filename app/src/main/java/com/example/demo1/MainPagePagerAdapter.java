package com.example.demo1;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class MainPagePagerAdapter extends FragmentPagerAdapter {

    // Constructor
    public MainPagePagerAdapter(@NonNull FragmentManager fm) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    // Overridden FragmentPagerAdapter methods
    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new MainPageLeftFragment();
                break;
            case 1:
                fragment = new MainPageFragment();
                break;
            case 2:
                fragment = new MainPageRightFragment();
                break;
        }

        return fragment;

    }

    @Override
    public int getCount() {
        return numPages;
    }

    // Private properties
    final private int numPages = 3;
}
