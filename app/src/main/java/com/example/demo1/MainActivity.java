package com.example.demo1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    // Overridden AppCompatActivity methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Link XML elements to properties
        alertsSelectedIndicator = findViewById(R.id.alerts_selected);
        historySelectedIndicator = findViewById(R.id.history_selected);
        alertsButton = findViewById(R.id.alerts_button);
        snapButton = findViewById(R.id.snap_button);
        historyButton = findViewById(R.id.history_button);

        // Hide button indicators
        alertsSelectedIndicator.setVisibility(View.INVISIBLE);
        historySelectedIndicator.setVisibility(View.INVISIBLE);

        // Set up viewPager
        viewPager = findViewById(R.id.viewPager);
        if (viewPager != null) {
            MainPagePagerAdapter adapter = new MainPagePagerAdapter(getSupportFragmentManager());
            viewPager.setAdapter(adapter);
        }

        // Listen to page changes
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Do nothing
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        // Alerts page selected
                        alertsSelectedIndicator.setVisibility(View.VISIBLE);
                        historySelectedIndicator.setVisibility(View.INVISIBLE);
                        break;
                    case 2:
                        // History page selected
                        alertsSelectedIndicator.setVisibility(View.INVISIBLE);
                        historySelectedIndicator.setVisibility(View.VISIBLE);
                        break;
                    default:
                        // All other pages selected
                        alertsSelectedIndicator.setVisibility(View.INVISIBLE);
                        historySelectedIndicator.setVisibility(View.INVISIBLE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Do nothing
            }
        });

        // Alerts button selected
        alertsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Smooth scroll to Alerts page
                viewPager.setCurrentItem(0, true);
            }
        });

        // Snap button selected
        snapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() != 1) {
                    // If current page is not Home, then smooth scroll to Home
                    viewPager.setCurrentItem(1, true);
                }
                else {
                    // Already on Home page, snap button
                    Log.d("MainActivity", "Snap button tapped");
                }
            }
        });

        // History button selected
        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Smooth scroll to History page
                viewPager.setCurrentItem(2, true);
            }
        });

        // Jump to Home page
        viewPager.setCurrentItem(1, false);
    }

    // Private properties
    ViewPager viewPager = null;
    ImageButton alertsButton = null;
    ImageButton snapButton = null;
    ImageButton historyButton = null;
    ImageView alertsSelectedIndicator = null;
    ImageView historySelectedIndicator = null;
}
